name "dbserver"
description "SQL server DB"
run_list "recipe[sql_server]"
default_attributes "sql_server" => {"accept_eula" => true}